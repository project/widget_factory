<?php

/**
 * @file
 *   widget_factory.install
 *   Install file for widget_factory module.
 */

/**
 * Implementation of hook_schema().
 */
function widget_factory_schema() {
  $schema['widget_factory_preset'] = array(
    'description' => t('Table storing preset definitions.'),
    'export' => array(
      'key' => 'name',
      'key name' => 'Name',
      'primary key' => 'pid',
      'identifier' => 'preset', // Exports will be defined as $preset
      'default hook' => 'default_widget_factory_preset',  // Function hook name.
      'api' => array(
        'owner' => 'widget_factory',
        'api' => 'default_widget_factory_presets',  // Base name for api include files.
        'minimum_version' => 1,
        'current_version' => 1,
      ),
    ),
    'fields' => array(
      'name' => array(
        'type' => 'varchar',
        'length' => '255',
        'description' => 'Unique ID for presets. Used to identify them programmatically.',
      ),
      'pid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Primary ID field for the table. Not used for anything except internal lookups.',
        'no export' => TRUE, // Do not export database-only keys.
      ),
      'widget_type' => array(
        'type' => 'varchar',
        'length' => '255',
        'description' => 'The type of widget.',
      ),
      'description' => array(
        'type' => 'varchar',
        'length' => '255',
        'description' => 'A human readable name of a preset.',
      ),
      'widget_settings' => array(
        'type' => 'blob',
        'serialize' => TRUE,
        'description' => 'Serialzed settings for a widget.',
      ),
    ),
    'primary key' => array('pid'),
    'unique keys' => array(
      'name' => array('name'),
    ),
  );
  return $schema;
}
