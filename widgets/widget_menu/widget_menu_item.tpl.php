<li class="<?php echo $link['class']; ?>">
  <a  href="<?php echo $link['href']; ?>">
    <i class="fa <?php echo $link['fa_icon'] ?> fa-fw"></i> <?php echo $link['title']; ?>
  </a>
</li>
