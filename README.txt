CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------
The Widget Factory module helps you manage various widget created by modules
that uses its api.


 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/widget_factory


 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/widget_factory

RECOMMENDED MODULES
-------------------
 * PM Kickstart Theme (https://www.drupal.org/project/pm_kickstart_theme):
   This theme supports default widgets thats comes along with widget_factory.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Following js libraries are needed.
  angular-poller.js from https://github.com/emmaguo/angular-poller/
  angular.js and angular-resource.js from  http://code.angularjs.org/

  put them in libraries folder so that it looks something like below

  sites/all/libraries/angular-poller/angular-poller.js
  sites/all/libraries/angular/angular-resource.js
  sites/all/libraries/angular/angular.js


CONFIGURATION
-------------
 * Configure user permissions in Administration » People » Permissions:


   - Administer Widget Factory (Widget Factory)


     Perform administartion tasks for Widget Factory


 * Manage widgets in Administration » Structure » Widget Factory.


 * Widgets will be available as Drupal Blocks. You can manage them at
   Administration » Structure » Blocks

   or use modules like Homebox/Context UI/Panels to show them.

TROUBLESHOOTING
---------------
@TODO

FAQ
---

@TODO

MAINTAINERS
-----------
Current maintainers:
 * Shibin Das (D34dMan) - https://www.drupal.org/user/751698

